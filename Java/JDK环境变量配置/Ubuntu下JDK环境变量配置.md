# Ubuntu下JDK环境变量配置

## 1. JDK下载

在[Oracle官网](http://www.oracle.com/technetwork/java/javase/downloads/index.html)下载JDK：jdk-10.0.1_linux-x64_bin.tar.gz

![JDK下载](./imgs/2afd4c17208749199e068e6b3d9d30b7.png)

## 2. 解压jdk

在/usr/local目录下新建java目录。命令：
```
sudo mkdir /usr/local/java
```
将JDK压缩包复制到/usr/local/java下，并且解压。命令：
```
sudo cp jdk-10.0.1_linux-x64_bin.tar.gz /usr/local/java
cd /usr/local/java
sudo tar zxvf jdk-10.0.1_linux-x64_bin.tar.gz
```

## 3. 设置环境变量

这里采用全局设置的方式。编辑`/etc/profile`文件。命令：
```
sudo vim /etc/profile
```
在`/etc/profile`文件最底部，添加如下内容：
```
export JAVA_HOME=/usr/local/java/jdk-10.0.1
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH
```

## 4. 生效环境变量

命令：
```
sudo . /etc/profile
```

## 5. 校验是否配置成功

命令：

```
$ java --version
java 10.0.1 2018-04-17
Java(TM) SE Runtime Environment 18.3 (build 10.0.1+10)
Java HotSpot(TM) 64-Bit Server VM 18.3 (build 10.0.1+10, mixed mode)
```